﻿﻿
@Code
    Layout = Nothing
End Code
<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width" />
    <link href="~/scripts/css/bootstrap.min.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Quicksand|Rubik" rel="stylesheet">
    <style>

        table {
            font-family: 'Quicksand', sans-serif;
        }

        .sidenav {
            height: 100%;
            width: 0;
            position: fixed;
            z-index: 1;
            top: 0;
            left: 0;
            background-color: darkred;
            overflow-x: hidden;
            transition: 0.5s;
            padding-top: 60px;
        }

            .sidenav a {
                padding: 8px 8px 8px 32px;
                text-decoration: none;
                font-size: 25px;
                color: #818181;
                display: block;
                transition: 0.3s;
            }

                .sidenav a:hover {
                    color: #f1f1f1;
                }

            .sidenav .closebtn {
                position: absolute;
                top: 0;
                right: 25px;
                font-size: 36px;
                margin-left: 50px;
            }

        #main {
            transition: margin-left .5s;
            padding: 16px;
        }


        table {
            font-family: 'Quicksand', sans-serif;
        }

            table .w-30 {
                width: 30%;
            }

        body {
            font-family: 'Niramit', sans-serif;
            background-color: #cccccc;
            transition: background-color .5s;
        }

        .bg-company-red {
            background-color: darkred;
        }

        body {
            font-family: 'Rubik', sans-serif;
            background-image: url("~/Resources/fondo.jpg");
            background-color: #cccccc;
        }
    </style>
    <title>R.T. - Reserva </title>
</head>
<body>
    <nav class="navbar navbar-expand-sm bg-company-red">
        <!-- Brand/logo -->
        <a class="navbar-brand" href="#">
            <img class="pull-right" src="~/Resources/logoUaa.jpg" alt="logo" style="width:40px;">
        </a>

        <span>
            <span class="navbar-text form-inline" style="color:white;">Registro de Tutoria</span>
        </span>
    </nav>

    <div id="mySidenav" class="sidenav">
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
        <a href="#">Inicio</a>
        <a href="#">Alumno</a>
        <a href="~/Reserva/ListarReservas?ci=@ViewData("cedula")">Mis reservas</a>
        <a href="#">Contacto</a>
    </div>
    <script>
        function openNav() {
            document.getElementById("mySidenav").style.width = "250px";
            document.getElementById("main").style.marginLeft = "250px";
            document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
        }

        function closeNav() {
            document.getElementById("mySidenav").style.width = "0";
            document.getElementById("main").style.marginLeft = "0";
            document.body.style.backgroundColor = "#cccccc";
        }
    </script>

    <div id="main">
        <span style="font-size:30px;cursor:pointer" onclick="openNav()">Menu</span>
        <div class="text-center">
            <h3 class="h3">Reserva - Paso 1</h3>
            <h4 class="h4">Listado de disponbilidad de todos los empleados por dia</h4>
            <div class="btn-group">
                <div class="form-group row">
                    Seleccione un día: <label for="cliente" class="col-sm-2 col-form-label">Dia</label>
                    <div class="col-sm-12 col-lg-4">
                        <select class="form-control" id="cboDia" name="cboDia" onchange="ConsultarDias()" style="width:150px;">
                            <option selected value="Lunes">Lunes</option>
                            <option value="Martes">Martes</option>
                            <option value="Miercoles">Miercoles</option>
                            <option value="Jueves">Jueves</option>
                            <option value="Viernes">Viernes</option>
                        </select>
                    </div>
                </div>
                @*<a href="~/Alumno/EditStudent" class="btn btn-success">Modificar Datos</a>*@
            </div>
            <br />
            <br />
        </div>
        <Table Class="table table-striped table-hover table-bordered" style="width:800px; margin:auto;">
            <thead>
                <tr>
                    <th> Empleado</th>
                    <th> Dia</th>
                    <th> Inicio</th>
                    <th> Fin</th>
                </tr>
            </thead>
            <tbody id="planilla"></tbody>
        </Table>
        </div>
        <script src="~/scripts/jquery-3.3.1.min.js"></script>
        <script src="~/scripts/js/bootstrap.min.js"></script>
        <script type="text/javascript">
    function ConsultarDias() {

                var parametro = { dia: $("#cboDia").val() };



                $.ajax({
                    type: "POST",
                    url: '/Reserva/ObtenerDiasDisponibles',
                    data: parametro,
                    dataType: "json",
                    success: function (msg) {

                        var datos = jQuery.parseJSON(msg);
                        var row = "";
                        for (i = 0; i < datos.length; i++) {
                            row += "<tr>" +
                                "<td>" + datos[i].NombreApellido +
                                "</td><td>" + datos[i].Dia +
                                "</td><td>" + datos[i].Inicio +
                                "</td><td>" + datos[i].Fin +
                                "</td><td><a href='../../Reserva/Procesar/?empleado=" + datos[i].CodigoEmpleado + "&disp=" + datos[i].CodigoDisponibilidad + "&dia=" + $("#cboDia").val() + "&ci=@ViewData("cedula")'>Seleccionar</a> </td></tr>";
                        }
                        $("#planilla").html(row);
                    },
                    error: function () {
                        alert("se ha producido un error cargar planilla.");
                    }
                });

            }</script>
</body>
</html>
