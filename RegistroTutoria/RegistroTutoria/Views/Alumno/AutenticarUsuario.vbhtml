﻿
@Code
    Layout = Nothing
End Code

<!DOCTYPE html>

<html>
<head>
    <meta name="viewport" content="width=device-width" />
    <link href="~/scripts/css/bootstrap.min.css" rel="stylesheet" />
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Niramit" rel="stylesheet">

    <style>
        .accordion {
            background-color: #eee;
            color: #444;
            cursor: pointer;
            padding: 18px;
            width: 100%;
            border: none;
            text-align: left;
            outline: none;
            font-size: 15px;
            transition: 0.4s;
        }

            .active, .accordion:hover {
                background-color: #993300;
                color: white;
            }

        .panel {
            padding: 0 18px;
            display: none;
            background-color: white;
            overflow: hidden;
        }

        .sidenav {
            height: 100%;
            width: 0;
            position: fixed;
            z-index: 1;
            top: 0;
            left: 0;
            background-color: darkred;
            overflow-x: hidden;
            transition: 0.5s;
            padding-top: 60px;
        }

            .sidenav a {
                padding: 8px 8px 8px 32px;
                text-decoration: none;
                font-size: 25px;
                color: #818181;
                display: block;
                transition: 0.3s;
            }

                .sidenav a:hover {
                    color: #f1f1f1;
                }

            .sidenav .closebtn {
                position: absolute;
                top: 0;
                right: 25px;
                font-size: 36px;
                margin-left: 50px;
            }

        #main {
            transition: margin-left .5s;
            padding: 16px;
        }


        table {
            font-family: 'Quicksand', sans-serif;
        }

        a {
            text-decoration: none;
        }

            a:hover {
                text-decoration: none;
            }

        table .w-30 {
            width: 30%;
        }

        body {
            font-family: 'Niramit', sans-serif;
            background-color: #cccccc;
            transition: background-color .5s;
        }

        .bg-company-red {
            background-color: darkred;
        }

        .nav {
            text-align: right;
        }
        .navbar-text{
            color:white;
        }
    </style>
</head>
<body>

    <nav class="navbar navbar-expand-md navbar-dark bg-company-red">
        <div class="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2">
            <ul class="navbar-nav mr-auto">
                <li class="navbar-brand">
                    <img class="pull-right" src="~/Resources/logoUaa.jpg" alt="logo" style="width:40px;">
                </li>

            </ul>
        </div>
        <div class="mx-auto order-0">
            <a class="navbar-brand mx-auto" href="#">Registro de Tutoria</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".dual-collapse2">
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>
        <div class="navbar-collapse collapse w-100 order-3 dual-collapse2">
            <ul class="navbar-nav ml-auto">
                <!--
                <li class="nav-item">
                    <a class="nav-link" href="#">Right</a>
                </li>-->
                <li class="nav-item">
                    <p style="color:white;">Hola, @ViewData("Alumno")</p>
                </li>
            </ul>
        </div>
    </nav>



    <div id="mySidenav" class="sidenav">
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
        <a href="#">Inicio</a>
        <a href="#">Alumno</a>
        <a href="~/Reserva/ListarReservas?ci=@ViewData("Cedula")">Mis reservas</a>
        <a href="#">Contacto</a>
    </div>
    <script>
        function openNav() {
            document.getElementById("mySidenav").style.width = "250px";
            document.getElementById("main").style.marginLeft = "250px";
            document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
        }

        function closeNav() {
            document.getElementById("mySidenav").style.width = "0";
            document.getElementById("main").style.marginLeft = "0";
            document.body.style.backgroundColor = "#cccccc";
        }
    </script>
    <div id="main">

        <span style="font-size:30px;cursor:pointer" onclick="openNav()">Menu</span>
        <div style="width:900px;margin:auto" class="text-center">
            <button class="accordion">Hacer Reserva</button>
            <div class="panel">
                <a href="~/Reserva/IniciarReserva?ci=@ViewData("Cedula")" style="color:inherit;">Seleccione un dia y luego un profesor para hacer su reserva de tutoria.</a>
            </div>

            <button class="accordion">Listar mis reservas</button>
            <div class="panel">
                <p>Liste todas sus proximas reservas</p>
            </div>

            <button class="accordion">Cancelar Reservas</button>
            <div class="panel">
                <p>Cancele sus reservas hechas.</p>
            </div>
        </div>
    </div>
    <script>
        var acc = document.getElementsByClassName("accordion");
        var i;

        for (i = 0; i < acc.length; i++) {
            acc[i].addEventListener("click", function () {
                this.classList.toggle("active");
                var panel = this.nextElementSibling;
                if (panel.style.display === "block") {
                    panel.style.display = "none";
                } else {
                    panel.style.display = "block";
                }
            });
        }
    </script>
</body>
</html>
