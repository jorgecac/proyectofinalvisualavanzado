﻿Imports System.Web.Mvc

Namespace Controllers
    Public Class HomeController
        Inherits Controller

        ' GET: Home
        Function Index() As ActionResult
            Dim dbServer, dbUser, dbPass, dbName As String
            ''System.Configuration.ConfigurationManager.AppSettings["AdminPassword"].ToString()

            dbServer = ConfigurationManager.AppSettings("DBServer").ToString
            dbName = ConfigurationManager.AppSettings("DBName").ToString
            dbPass = ConfigurationManager.AppSettings("DBPassword").ToString
            dbUser = ConfigurationManager.AppSettings("DBUser").ToString

            Try
                Util.inicializaSesion(dbServer, dbName, dbUser, dbPass)
            Catch ex As Exception
                Throw ex
            End Try

            Return View()
        End Function


        Function BackToIndex(alumno As String, cedula As String) As ActionResult
            ViewData("Alumno") = alumno
            ViewData("Cedula") = cedula
            Return View()

        End Function
    End Class
End Namespace