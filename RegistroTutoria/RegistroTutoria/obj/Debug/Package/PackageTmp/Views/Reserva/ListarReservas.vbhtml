﻿
@Code
    Layout = Nothing
End Code

<!DOCTYPE html>

<html>
<head>
    <meta name="viewport" content="width=device-width" />
    <link href="~/scripts/css/bootstrap.min.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Quicksand|Rubik" rel="stylesheet">
    <style>

        table {
            font-family: 'Quicksand', sans-serif;
        }

        .sidenav {
            height: 100%;
            width: 0;
            position: fixed;
            z-index: 1;
            top: 0;
            left: 0;
            background-color: darkred;
            overflow-x: hidden;
            transition: 0.5s;
            padding-top: 60px;
        }

            .sidenav a {
                padding: 8px 8px 8px 32px;
                text-decoration: none;
                font-size: 25px;
                color: #818181;
                display: block;
                transition: 0.3s;
            }

                .sidenav a:hover {
                    color: #f1f1f1;
                }

            .sidenav .closebtn {
                position: absolute;
                top: 0;
                right: 25px;
                font-size: 36px;
                margin-left: 50px;
            }

        #main {
            transition: margin-left .5s;
            padding: 16px;
        }


        table {
            font-family: 'Quicksand', sans-serif;
        }

            table .w-30 {
                width: 30%;
            }

        body {
            font-family: 'Niramit', sans-serif;
            background-color: #cccccc;
            transition: background-color .5s;
        }

        .bg-company-red {
            background-color: darkred;
        }

        body {
            font-family: 'Rubik', sans-serif;
            background-image: url("~/Resources/fondo.jpg");
            background-color: #cccccc;
        }
    </style>
    <title>R.T. - Reserva </title>
</head>
<body>
    <nav class="navbar navbar-expand-sm bg-company-red">
        <!-- Brand/logo -->
        <a class="navbar-brand" href="#">
            <img class="pull-right" src="~/Resources/logoUaa.jpg" alt="logo" style="width:40px;">
        </a>

        <span>
            <span class="navbar-text form-inline" style="color:white;">Registro de Tutoria</span>
        </span>
    </nav>

    <div id="mySidenav" class="sidenav">
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
        <a href="#">Inicio</a>
        <a href="#">Alumno</a>
        <a href="#">Mis reservas</a>
        <a href="#">Contacto</a>
    </div>
    <script>
        function openNav() {
            document.getElementById("mySidenav").style.width = "250px";
            document.getElementById("main").style.marginLeft = "250px";
            document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
        }

        function closeNav() {
            document.getElementById("mySidenav").style.width = "0";
            document.getElementById("main").style.marginLeft = "0";
            document.body.style.backgroundColor = "#cccccc";
        }
    </script>

    <div id="main">
        <span style="font-size:30px;cursor:pointer" onclick="openNav()">Menu</span>
        <div class="text-center">
            <h4 class="h4">Hola, @ViewData("alumno"), estas son tus reservas</h4>
            <h4 class="h4">Listado de reservas que realizaste</h4>
            <input type="text" value="@ViewData("ci")" id="ci"/>
            <input type="submit" onclick="ConsultarReservas()" class="btn-primary" value="Listar"/>
            <br />
            <br />
        </div>

        <Table Class="table table-striped table-hover table-bordered" style="width:800px; margin:auto;">
            <thead>
                <tr>
                    <th> Fecha</th>
                    <th> Hora</th>
                    <th> Profesor</th>
                    <th> Motivo</th>
                </tr>
            </thead>
            <tbody id="planilla"></tbody>
        </Table>
    </div>
    <script src="~/scripts/jquery-3.3.1.min.js"></script>
    <script src="~/scripts/js/bootstrap.min.js"></script>
    <script type="text/javascript">
    function ConsultarReservas() {
        
        var cedula = $("#ci").val();
        var parametro = { ci:cedula};
        alert("CI " + cedula)
                $.ajax({
                    type: 'POST',
                    url: '/Reserva/ListarReservasPorAlumno',
                    data: parametro,
                    dataType: 'json',
                    success: function (msg) {
                        
                        var datos = jQuery.parseJSON(msg);
                        var row = "";
                        for (i = 0; i < datos.length; i++) {
                            row += "<tr>" +
                                "<td>" + datos[i].Fecha +
                                "</td><td>" + datos[i].Hora+
                                "</td><td>" + datos[i].Profesor+
                                "</td><td>" + datos[i].Motivo +"</td></tr>";
                        }
                        $("#planilla").html(row);
                    },
                    error: function () {
                        alert("se ha producido un error cargar planilla.");
                    }
                });

            }</script>
</body>
</html>
